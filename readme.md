# Notes to Nasdaq web team

This site is built using Foundation 6. If you need to use the build tools, read below for Foundation documentation. I recommended using the CLI tool to build production files.

If you need anything, please email me at dpogni@gmail.com.

# Nasdaq TODOs

Off the top of my head, here's some top items for the Nasdaq team.

[] Content feeds with icons
[] Forms
    [] /investor-resources/investor-information-request.html
    [] /contact-us.html
    [] /corporate-governance.html
    [] /investor-resources/email-alerts.html
    [] ... there might be more, which are connected to content feeds / widgets
[] Footer stock ticker
[] Charts/widgets
    [] /stock-information.html
    [] /stock-information/investment-calculator.html
    [] /stock-information/historic-stock-lookup.html
[] Connect the shareholder tools links

# ZURB Template

[![devDependency Status](https://david-dm.org/zurb/foundation-zurb-template/dev-status.svg)](https://david-dm.org/zurb/foundation-zurb-template#info=devDependencies)

**Please open all issues with this template on the main [Foundation for Sites](https://github.com/zurb/foundation-sites/issues) repo.**

This is the official ZURB Template for use with [Foundation for Sites](http://foundation.zurb.com/sites). We use this template at ZURB to deliver static code to our clients. It has a Gulp-powered build system with these features:

- Handlebars HTML templates with Panini
- Sass compilation and prefixing
- JavaScript concatenation
- Built-in BrowserSync server
- For production builds:
  - CSS compression
  - JavaScript compression
  - Image compression

## Installation

To use this template, your computer needs:

- [NodeJS](https://nodejs.org/en/) (0.12 or greater)
- [Git](https://git-scm.com/)

This template can be installed with the Foundation CLI, or downloaded and set up manually.

### Using the CLI (recommended)

Install the Foundation CLI with this command:

```bash
npm install foundation-cli --global
```

Build project files for production to dist folder

```bash
foundation build
```

For code watcher and live reload run:

```bash
foundation watch
```
